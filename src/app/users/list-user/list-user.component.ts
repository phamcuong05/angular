import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  listUsers: any;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.listUsers = this.userService.listUsers().subscribe(data => {
      this.listUsers = data;
    });
  }

}
